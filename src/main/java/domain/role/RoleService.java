package domain.role;

public interface RoleService {

    Role saveRole(Role role);

    Role findByRoleName(String roleName);
}
