package domain.role;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class RoleServiceImpl implements  RoleService{

    private  final RoleRepository repository;

    @Override
    public Role saveRole(Role role) {
        log.info("Salvando permissão");
        return repository.save(role);
    }

    @Override
    public Role findByRoleName(String roleName) {
        log.info("Buscando uma permissao por nome");
        return repository.findByName(roleName);
    }
}
