package domain.user;

import domain.role.Role;
import domain.role.RoleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository repository;

    RoleService roleService;

    @Override
    public User saveUser(User user) {
        return repository.save(user);
    }

    @Override
    public void addRoleToUser(String username, String roleName) {
        log.info("Adicionando permissao para o usuario {}", roleName, username);
        User user = repository.findByUsername(username);
        Role role = roleService.findByRoleName(roleName);
        user.getRoles().add(role);
    }

    @Override
    public User getUser(String username) {
        log.info("Obter usuario por nome {}",  username);
        return repository.findByUsername(username);
    }

    @Override
    public List<User> getUsers() {
        return repository.findAll();
    }
}
